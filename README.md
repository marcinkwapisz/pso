## Particle Swarm Optimization
This project was written using .NET Framework 4.6.1.

It consists of the self-written particle swarm optimization code and genetic algorithm code from [GeneticSharp library](https://github.com/giacomelli/GeneticSharp).

The main goal was to find minimum values of functions of several variables (Rosenbrock, Griewank, Rastrigin) and comparison of the effectiveness and results of PSO and GA.
