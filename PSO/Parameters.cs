﻿using PSO.Data;

namespace PSO
{
    public class Parameters
    {
        public int NumberOfRepetitions { get; set; }
        public FunctionType.Type Function { get; set; }
        public int NumberOfDimensions { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int NumberOfIterations { get; set; }
        public double Accuracy { get; set; }

        public int NumberOfParticles { get; set; }
        public double Weight { get; set; }
        public double CognitiveWeight { get; set; }
        public double SocialWeight { get; set; }
        public bool RandomWeight { get; set; }
        public bool ModificationVelocityEquation { get; set; }
        public AccelerationCoefficientMode.Mode AccelerationCoefficientReductionMode { get; set; }

        public int PopulationSize { get; set; }
        public float CrossoverProbability { get; set; }
        public float MutationProbability { get; set; }
        public float UniformCrossoverProbabilty { get; set; }

        public Parameters(int numberOfRepetitions, FunctionType.Type function, int numberOfDimensions,
            double minValue, double maxValue, int numberOfIterations, double accuracy,
            int numberOfParticles, double weight, double cognitiveWeight, double socialWeight,
            bool randomWeight, bool modificationVelocityEquation, AccelerationCoefficientMode.Mode accelerationCoefficientReductionMode)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            NumberOfParticles = numberOfParticles;
            Weight = weight;
            CognitiveWeight = cognitiveWeight;
            SocialWeight = socialWeight;
            RandomWeight = randomWeight;
            ModificationVelocityEquation = modificationVelocityEquation;
            AccelerationCoefficientReductionMode = accelerationCoefficientReductionMode;
        }

        public Parameters(int numberOfRepetitions, FunctionType.Type function, int numberOfDimensions, 
            double minValue, double maxValue, int numberOfIterations, double accuracy, int populationSize, 
            float crossoverProbability, float mutationProbability, float uniformCrossoverProbabilty)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            PopulationSize = populationSize;
            CrossoverProbability = crossoverProbability;
            MutationProbability = mutationProbability;
            UniformCrossoverProbabilty = uniformCrossoverProbabilty;
        }

        public Parameters()
        {
        }
    }
}
