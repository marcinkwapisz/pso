﻿using PSO.BusinessLogic;
using System;
using System.IO;
using System.Xml.Serialization;

namespace PSO.View
{
    class Launcher
    {
        enum Algorithm { GeneticAlgorithm, ParticleSwarmOptimization };
        static void Main(string[] args)
        {
            //Algorithm choice = Algorithm.GeneticAlgorithm;
            Algorithm choice = Algorithm.ParticleSwarmOptimization;
            if (choice.Equals(Algorithm.GeneticAlgorithm))
            {
                string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../GA_parameters.xml";
                Parameters retrievedParameters = new Parameters();
                using (FileStream fileStream = new FileStream(parametersPath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(retrievedParameters.GetType());
                    retrievedParameters = serializer.Deserialize(fileStream) as Parameters;
                }
                GeneticAlgorithmService geneticAlgorithmService = new GeneticAlgorithmService(retrievedParameters);
                for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
                {
                    Console.WriteLine(geneticAlgorithmService.OptimizeWithGA());
                }
                geneticAlgorithmService.SaveResultsOfGeneticAlgorithm();
            }
            else if (choice.Equals(Algorithm.ParticleSwarmOptimization))
            {
                string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../PSO_parameters.xml";
                Parameters retrievedParameters = new Parameters();
                using (FileStream fileStream = new FileStream(parametersPath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(retrievedParameters.GetType());
                    retrievedParameters = serializer.Deserialize(fileStream) as Parameters;
                }

                ParticleService particleService = new ParticleService(retrievedParameters);
                for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
                {
                    Console.WriteLine(particleService.OptimizeWithPSO());
                }
                particleService.SaveResultsOfOptimization();
            }
        }
    }
}
