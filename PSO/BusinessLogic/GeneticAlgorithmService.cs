﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using System.Collections.Generic;

namespace PSO.BusinessLogic
{
    class GeneticAlgorithmService
    {
        private readonly Parameters gaParameters;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();
        GeneticAlgorithm geneticAlgorithm;
        List<double> listForAverageFitnessInIteration;

        public GeneticAlgorithmService(Parameters parameters)
        {
            gaParameters = parameters;
        }

        public void InitializeVariables()
        {
            listForAverageFitnessInIteration = new List<double>();
            double[] minValues = new double[gaParameters.NumberOfDimensions];
            double[] maxValues = new double[gaParameters.NumberOfDimensions];
            int[] totalBits = new int[gaParameters.NumberOfDimensions];
            int[] fractionDigits = new int[gaParameters.NumberOfDimensions];
            for (int i = 0; i < gaParameters.NumberOfDimensions; i++)
            {
                minValues[i] = gaParameters.MinValue;
                maxValues[i] = gaParameters.MaxValue;
                totalBits[i] = 64;
                fractionDigits[i] = 16;
            }
            IChromosome chromosome = new FloatingPointChromosome(minValues, maxValues, totalBits, fractionDigits);
            IPopulation population = new Population(gaParameters.PopulationSize, gaParameters.PopulationSize, chromosome);
            IFitness fitness = new FuncFitness((inputChromosome) =>
            {
                FloatingPointChromosome floatingPointChromosome = inputChromosome as FloatingPointChromosome;
                double[] values = floatingPointChromosome.ToFloatingPoints();
                return 1 / ObjectiveFunction.GetFitnessToObjectiveFunction(gaParameters.Function, values);  // 1/result -> min, result -> max
            });
            ISelection selection = new EliteSelection();
            ICrossover crossover = new UniformCrossover(gaParameters.UniformCrossoverProbabilty);
            IMutation mutation = new FlipBitMutation();
            ITermination termination = new GenerationNumberTermination(gaParameters.NumberOfIterations);
            geneticAlgorithm = new GeneticAlgorithm(population, fitness, selection, crossover, mutation)
            {
                CrossoverProbability = gaParameters.CrossoverProbability,
                MutationProbability = gaParameters.MutationProbability,
                Termination = termination
            };
        }

        public int OptimizeWithGA()
        {
            InitializeVariables();
            AddHandleToGeneticAlgorithmEvents();
            geneticAlgorithm.Start();
            return geneticAlgorithm.GenerationsNumber;
        }

        private void AddHandleToGeneticAlgorithmEvents()
        {
            geneticAlgorithm.GenerationRan += GeneticAlgorithm_GenerationRan;
            geneticAlgorithm.Stopped += GeneticAlgorithm_Stopped;
            geneticAlgorithm.TerminationReached += GeneticAlgorithm_TerminationReached;
        }

        private void GeneticAlgorithm_GenerationRan(object sender, EventArgs e)
        {
            double averageFitness = CalculateFitnessAverageValue(geneticAlgorithm.Population.CurrentGeneration.Chromosomes);
            listForAverageFitnessInIteration.Add(averageFitness);
            if (averageFitness < gaParameters.Accuracy)
                geneticAlgorithm.Stop();
        }

        private void GeneticAlgorithm_Stopped(object sender, EventArgs e)
        {
            AddResultsToBeStored();
            results.IfOptimizationWasSuccessful.Add(true);
        }

        private void GeneticAlgorithm_TerminationReached(object sender, EventArgs e)
        {
            AddResultsToBeStored();
            results.IfOptimizationWasSuccessful.Add(false);

        }

        private void AddResultsToBeStored()
        {
            if (geneticAlgorithm.GenerationsNumber < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = listForAverageFitnessInIteration;
                bestAmountOfIterations = geneticAlgorithm.GenerationsNumber;
            }
            results.NumberOfIterations.Add(geneticAlgorithm.GenerationsNumber);
        }

        public void SaveResultsOfGeneticAlgorithm()
        {
            results.SaveResultsToXml("GA");
        }

        private double CalculateFitnessAverageValue(IList<IChromosome> chromosomes)
        {
            double sum = 0.0;
            for (int i = 0; i < chromosomes.Count; i++)
            {
                FloatingPointChromosome chromose = chromosomes[i] as FloatingPointChromosome;
                double[] phenotype = chromose.ToFloatingPoints();
                sum += ObjectiveFunction.GetFitnessToObjectiveFunction(gaParameters.Function, phenotype);
            }
            return sum / chromosomes.Count;
        }
    }
}
