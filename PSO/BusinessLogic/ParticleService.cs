﻿using PSO.Data;
using System;
using System.Collections.Generic;

namespace PSO.BusinessLogic
{
    class ParticleService
    {
        private readonly Parameters psoParameters;
        private Particle[] swarm;
        private double[] bestGlobalPosition;
        private double bestGlobalFitness;
        private Random random = new Random();
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();
        private double cognitiveWeight;
        private double socialWeight;

        public ParticleService(Parameters parameters)
        {
            psoParameters = parameters;
        }

        private void InitializeSwarm()
        {
            swarm = new Particle[psoParameters.NumberOfParticles];
            bestGlobalPosition = new double[psoParameters.NumberOfDimensions];
            bestGlobalFitness = double.MaxValue;

            for (int i = 0; i < swarm.Length; i++)
            {
                double[] randomPosition = new double[psoParameters.NumberOfDimensions];
                double[] randomVelocity = new double[psoParameters.NumberOfDimensions];
                for (int j = 0; j < psoParameters.NumberOfDimensions; j++)
                {
                    randomPosition[j] = GetRandomNumberFromRange(psoParameters.MinValue, psoParameters.MaxValue);
                    randomVelocity[j] = GetRandomNumberFromRange(psoParameters.MinValue * 0.1, psoParameters.MaxValue * 0.1);
                }
                double fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(psoParameters.Function, randomPosition);

                swarm[i] = new Particle(randomPosition, randomVelocity, fitness, randomPosition, fitness);
                if (swarm[i].Fitness < bestGlobalFitness)
                {
                    bestGlobalFitness = swarm[i].Fitness;
                    swarm[i].Position.CopyTo(bestGlobalPosition, 0);
                }
            }
            cognitiveWeight = psoParameters.CognitiveWeight;
            socialWeight = psoParameters.SocialWeight;
        }

        public int OptimizeWithPSO()
        {
            InitializeSwarm();
            int iteration = 0;
            bool continueOptimization = true;
            List<double> listForAverageFitnessInIteration = new List<double>();
            while (iteration < psoParameters.NumberOfIterations && continueOptimization)
            {
                for (int i = 0; i < swarm.Length; i++)
                {
                    UpdateVelocityOfParticle(swarm[i]);
                    UpdatePositionOfParticle(swarm[i]);
                    CalculateFitnessOfParticle(swarm[i]);
                }
                double averageFitnessInIteration = CalculateFitnessAverageValue();
                listForAverageFitnessInIteration.Add(averageFitnessInIteration);
                if (averageFitnessInIteration < psoParameters.Accuracy)
                {
                    results.IfOptimizationWasSuccessful.Add(true);
                    continueOptimization = false;
                    break;
                }
                iteration++;
                switch (psoParameters.AccelerationCoefficientReductionMode)
                {
                    case AccelerationCoefficientMode.Mode.NoOne:
                        break;
                    case AccelerationCoefficientMode.Mode.OnlyCognitive:
                        cognitiveWeight -= 0.001;
                        break;
                    case AccelerationCoefficientMode.Mode.OnlySocial:
                        socialWeight -= 0.001;
                        break;
                    case AccelerationCoefficientMode.Mode.CognitiveAndSocial:
                        cognitiveWeight -= 0.001;
                        socialWeight -= 0.001;
                        break;
                }
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = listForAverageFitnessInIteration;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            if (continueOptimization)
                results.IfOptimizationWasSuccessful.Add(false);
            return iteration;
        }

        private void UpdateVelocityOfParticle(Particle particle)
        {
            double[] newVelocity = new double[particle.Velocity.Length];
            for (int j = 0; j < particle.Velocity.Length; j++)
            {
                double random1 = GetRandomNumberFromRange(0.0, 1.0);
                double random2 = GetRandomNumberFromRange(0.0, 1.0);
                if (psoParameters.RandomWeight)
                    psoParameters.Weight = GetRandomNumberFromRange(0.4, 0.9);
                if (!psoParameters.ModificationVelocityEquation)
                {
                    newVelocity[j] = (psoParameters.Weight * particle.Velocity[j]) +
                    (cognitiveWeight * random1 * (particle.BestPosition[j] - particle.Position[j])) +
                    (socialWeight * random2 * (bestGlobalPosition[j] - particle.Position[j]));
                }
                else
                {
                    newVelocity[j] = (psoParameters.Weight * particle.Velocity[j]) +
                    (cognitiveWeight * random1 * ((particle.BestPosition[j] + bestGlobalPosition[j]) / 2 - particle.Position[j])) +
                    (socialWeight * random2 * ((particle.BestPosition[j] - bestGlobalPosition[j]) / 2 - particle.Position[j]));
                }
            }
            newVelocity.CopyTo(particle.Velocity, 0);
        }

        private void UpdatePositionOfParticle(Particle particle)
        {
            double[] newPosition = new double[particle.Position.Length];
            for (int j = 0; j < particle.Position.Length; j++)
            {
                newPosition[j] = particle.Position[j] + particle.Velocity[j];
            }
            newPosition.CopyTo(particle.Position, 0);
        }

        private void CalculateFitnessOfParticle(Particle particle)
        {
            particle.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(psoParameters.Function, particle.Position);
            if (particle.Fitness < particle.BestFitness)
            {
                particle.BestFitness = particle.Fitness;
                particle.Position.CopyTo(particle.BestPosition, 0);
                if (particle.BestFitness < bestGlobalFitness)
                {
                    bestGlobalFitness = particle.BestFitness;
                    particle.BestPosition.CopyTo(bestGlobalPosition, 0);
                }
            }
        }

        private double GetRandomNumberFromRange(double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }

        private double CalculateFitnessAverageValue()
        {
            double sum = 0.0;
            for (int i = 0; i < swarm.Length; i++)
            {
                sum += swarm[i].Fitness;
            }
            return sum / swarm.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("PSO");
        }
    }
}
